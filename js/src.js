`use strict`;

/**
 * ###############################################
 * ######## SELECCIONAMOS ELEMENTOS A USAR #######
 * ###############################################
 */
// Seleccionamos las partes del html que vamos a utilizar en JS:
const button = document.querySelector(`button.llovera`);
const sectionUlOcho = document.querySelector(`section#simple`);
const ulOcho = document.querySelector(`ul.ochohoras`);
const ulFranja = document.querySelector(`ul.franjaHoraria`);

// --------------------------------------------------------------

/**
 * ###############################################
 * ######## CONVERTIMOS LA KEY EN CONSTANTE ######
 * ###############################################
 */
// Como nustra API requiere logearse para generar una key que usar con la url que hace la petición, para facilitarnos la vida, la almacenamos en una constante.
const KEY = `43dcc36cd07cd897e5ab7c6eb55df329`;

// --------------------------------------------------------------

/**
 * #########################################
 * ######## FUNCIONALIDAD DEL BOTÓN #######
 * ########################################
 */
// Vamos a dar funcionalidad al botón para que obtenga latitud y longitud:
button.addEventListener('click', () => {
  // Al hacer click, el botón redirige a la función getLocation para obtener la latitud y longitud.
  getLocation();
});

// --------------------------------------------------------------

/**
 * ############################################################
 * ######## FUNCIÓN QUE ARROJA LA LATITUD Y LA LONGITUD #######
 * ############################################################
 */
// Función que arroja la latitud y longitud de nuestro equipo.
// Esta función se activa al pulsar el botón de nuestra página.
// El restultado de esta función (latitud y longitud) será usado por la función que hace la petición a la API más abajo.
const getLocation = () => {
  const onUbicacionConcedida = (ubicacion) => {
    // Aqui ya tenemos acceso a la ubicación. Podemos seguir trabajando aqui dentro.
    const { latitude, longitude } = ubicacion.coords;

    getWeather(latitude, longitude);
  };

  const onErrorDeUbicacion = (err) => err;

  const opcionesDeSolicitud = {
    enableHighAccuracy: true, // Alta precisión.
    maximumAge: 0, // No queremos caché.
    timeout: 5000, // Esperar solo 5 segundos.
  };

  // Solicitar:
  navigator.geolocation.getCurrentPosition(
    onUbicacionConcedida,
    onErrorDeUbicacion,
    opcionesDeSolicitud
  );
};

// --------------------------------------------------------------

/**
 * ##############################################################
 * ######## FUNCIÓN ASÍNCRONA QUE SOLICITA DATOS A LA API #######
 * ##############################################################
 */
// Función asincrona que hace una petición al servidor API, usándo como parámetros la latitud y longitud obtenidas en la función "getLocation" que se activa al pulsar el botón de nuestrá página.:
async function getWeather(latitude, longitude) {
  // Hacemos un try catch para asegurarnos de que si ocurre algún error en la ejecución de todo el código de dentro del try se nos muestre por consola.
  try {
    // Enviamos una petición a la API indicando los siguientes parámetros:
    // Latitud y longitud del equipo que hace la petición.
    // Que muestre los resultados del tiempo en 3 franjas de 3 horas.
    // Insertando nuestra KEY para la API.
    // Indicando que la temperatura sea en grados celsius con unit=metric.
    // Indicando que el idioma sea el español.
    const response = await fetch(
      `https://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&cnt=3&appid=${KEY}&units=metric&lang=es`
    );

    /**
     * ######################################################
     * ###### OBTENIENDO RESPUESTA DE LA API ################
     * ######################################################
     */
    // Obtenemos la respuesta a la petición que le hicimos a la API y la almacenamos en la constante body.
    const body = await response.json();
    // Mostramos por consola la información de body (respuesta a la petición que le hicimos a la API).
    console.log(`body de la API:`, body);

    /**
     * #########################################
     * ######## MOSTRANDO DATOS DE LA API ######
     * #########################################
     */
    // Limpiamos los dos ul que hay en el html para asegurarnos de que estén vacíos antes de meterles la info que nosotros queramos:
    ulFranja.innerHTML = '';
    ulOcho.innerHTML = '';

    // Nos aseguramos de que el body de la API contenga información. En caso afirmativo continuamos.
    if (body) {
      const bodyList = body.list;
      console.log(`Predicción en 3 franjas horarias:`, bodyList);

      // ¿LLOVERÁ O NO LLOVERÁ?
      if (bodyList) {
        ulOcho.innerHTML = '<li>No lloverá.</li>';
        sectionUlOcho.classList.add(`sol`);
        for (const data of bodyList) {
          console.log(data.weather[0].main);
          if (data.weather[0].main === 'Rain') {
            ulOcho.innerHTML = '<li>Sí, lloverá.</li>';
            sectionUlOcho.classList.remove(`sol`);
            sectionUlOcho.classList.add(`lluvia`);
            break;
          }
        }

        // ¿CUÁNDO LLOVERÁ?
        const frag = document.createDocumentFragment();
        for (const data of bodyList) {
          if (data.weather[0].main === `Rain`) {
            const li = document.createElement(`li`);
            li.textContent = `☔ ​A partir de las ${data.dt_txt.slice(
              11,
              16
            )} ☔​`;
            frag.append(li);
          }
        }
        if (frag.children.length === 0) {
          const li = document.createElement(`li`);
          li.textContent = '​🌞​ No hay pronóstico de lluvia ​🌞​';
          frag.append(li);
        }

        ulFranja.append(frag);
      }
    }
  } catch (err) {
    // Lanzamos un error en caso de que haya sucedido algún problema en el desarrollo del código que está dentro del TRY.
    console.error(err);
  }
}
